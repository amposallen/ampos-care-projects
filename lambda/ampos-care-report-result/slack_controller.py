#!/usr/bin/python3

import json
import urllib.request
    
# Chieh Lee
#WEBHOOK_URL = 'https://hooks.slack.com/services/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'
WEBHOOK_URL = 'https://hooks.slack.com/services/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'
#SLACK_CHANNEL = '@Chieh'
#AUTHOR_USERNAME = 'MYSTERIOUS KPI BRO'

def post_result_with_json(output_dict, author_name):
    r = urllib.request.Request(url=WEBHOOK_URL)
    output_dict['username'] = author_name
    payload = json.dumps(output_dict)
    payload = payload.replace('"false"', 'false')
    payload = payload.replace('"true"', 'true')
    
    r.data = bytes(payload, 'utf-8')
    res = urllib.request.urlopen(r)
    user_json_text = res.read()
    return user_json_text

def post_result(streaming_string, author_name='乖乖'):
    r = urllib.request.Request(url=WEBHOOK_URL)
    print(streaming_string)
    
    payload = '{"username": "%s", "text": "%s"}' % (author_name, streaming_string)
    r.data = bytes(payload, 'utf-8')
    res = urllib.request.urlopen(r)
    user_json_text = res.read()
    return user_json_text