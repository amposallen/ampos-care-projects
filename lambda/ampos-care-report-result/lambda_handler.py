#!/usr/bin/python3

import sys
sys.path.append('lib')
import boto3
import json
import uuid
import smtplib
from datetime import datetime, timedelta, timezone
from copy import copy
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from slack_controller import post_result


BUCKET_NAME = 'customer-sync-sg.ampostech.com'
region = boto3.session.Session().region_name
s3_client = boto3.client('s3', region_name=region)
max_company_number = 2000

def get_all_reports():    
    res = s3_client.list_objects_v2(Bucket=BUCKET_NAME, Prefix='ampos-care-report', Delimiter='/archived')
    contents = list(reversed(sorted(res['Contents'], key=lambda x: x['LastModified'])))
    while 'NextContinuationToken' in res:
        res = s3_client.list_objects_v2(Bucket=BUCKET_NAME, Prefix='ampos-care-report', Delimiter='/archived', ContinuationToken=res['NextContinuationToken'])
        contents.extend(list(reversed(sorted(res['Contents'], key=lambda x: x['LastModified']))))

    #print(contents)
    
    def map_function(x):
        x.pop('ETag')
        x.pop('StorageClass')
        x['reportName'] = x['Key'].split('/')[-1].split('.')[0]
        survey_substr = datetime.strftime(x['LastModified'] + timedelta(hours=8), '%m%d') # TODO CAUTION**: depend on ampos-care survey naming convention
        survey_substr_today = datetime.strftime(datetime.now() + timedelta(hours=8), '%m%d') # TODO CAUTION**: depend on ampos-care survey naming convention
        survey_substr_yesterday = datetime.strftime(datetime.now() + timedelta(hours=8) - timedelta(days=1), '%m%d') # TODO CAUTION**: depend on ampos-care survey naming convention
        x['LastModified'] = datetime.strftime(x['LastModified'] + timedelta(hours=8), '%Y-%m-%d %H:%M:%S')        
        x['DownloadLink'] = s3_client.generate_presigned_url(ClientMethod='get_object', Params={'Bucket': BUCKET_NAME, 'Key': x['Key']}, ExpiresIn=86400)
        x['Size'] = f"{str(round(x['Size'] / 1000000, 3))} MB"
        if x['Key'].find('lesson_completion') > 0:
            x['reportType'] = 'Lesson'
            x['Latest'] = 'True'
        elif x['Key'].find('survey_result') > 0:
            x['reportType'] = 'Survey'
            if x['Key'].find(survey_substr_today) > -1 or x['Key'].find(survey_substr_yesterday) > -1:
                x['Latest'] = 'True'
            else:
                x['Latest'] = 'False'
        else:
            x['reportType'] = 'General'
            x['Latest'] = 'True'
        return x
    
    content = list(map(lambda x: map_function(x), contents))
    content = list(filter(lambda x: x['Key'] != 'ampos-care-report/', content))
    return content

def filter_latests():
    all_reports = get_all_reports()
    all_reports = sorted(all_reports, key=lambda x: (x['reportType'], x['Key']))
    return list(filter(lambda x: x['Latest'] == 'True' and x['Key'] != 'ampos-care-report/', all_reports))

def build_body_html_page(report_list):
    html_page = open('main.html', 'r').read()
    
    content = ''
    for r in report_list:
        download_link = r['DownloadLink']
        report_name = r['reportName']
        last_modified_date = r['LastModified']
        file_size = r['Size']        
        content += f'<div><a href="{download_link}">{report_name}</a></div>\n'

    html_filename = f"{uuid.uuid1()}.html"
    requested_page = open(f"/tmp/{html_filename}", 'w')
    requested_page.write(html_page.replace('{replacement_placeholder}', content))
    requested_page.close()
    return html_filename

def default_query_value(event):
    if not event['queryStringParameters']:
        event['queryStringParameters'] = {}
        event['queryStringParameters']['latest'] = 'false'
        event['queryStringParameters']['returnType'] = 'json'
    if 'latest' not in event['queryStringParameters']:
        event['queryStringParameters']['latest'] = 'false'
    if 'returnType' not in event['queryStringParameters']:
        event['queryStringParameters']['returnType'] = 'json'
    if 'token' not in event['queryStringParameters']:
        event['queryStringParameters']['token'] = 'dummy'

def check_token(evt):
    if evt['queryStringParameters']['token'] == "ampos123":
        return True
    else:
        return False

def content_type(evt):
    try:
        if evt['queryStringParameters']['returnType'] == 'html':
            return 'text/html'
        else:
            return 'application/json'
    except:
        return 'application/json'

def body(evt):
    try:
        if evt['queryStringParameters']['latest'] == 'true' and evt['queryStringParameters']['returnType'] == 'html':
            html_filename = build_body_html_page(filter_latests())
            html_page = open(f"/tmp/{html_filename}", 'r').read()
            return html_page
        elif evt['queryStringParameters']['latest'] == 'false' and evt['queryStringParameters']['returnType'] == 'html':
            html_filename = build_body_html_page(get_all_reports())
            html_page = open(f"/tmp/{html_filename}", 'r').read()
            return html_page
        elif evt['queryStringParameters']['latest'] == 'true' and evt['queryStringParameters']['returnType'] == 'json':
            print('Response: latest=true,returnType=json')
            return json.dumps(filter_latests(), indent=2, ensure_ascii=False, default=str)            
        elif evt['queryStringParameters']['latest'] == 'false' and evt['queryStringParameters']['returnType'] == 'json':
            print('Response: latest=false,returnType=json')
            return json.dumps(get_all_reports(), indent=2, ensure_ascii=False, default=str)            
    except Exception as e:
        print('Response: latest=false,returnType=json(exception)')
        print(e)
        return json.dumps(get_all_reports(), indent=2, ensure_ascii=False, default=str)

def send_email(email_list, html_filename):
    server = "email-smtp.us-east-1.amazonaws.com:587"
    send_from = "ampos-care@ampostech.com"
    send_to = email_list
    #send_to = ['chiehlee@ampostech.com']
    if email_list:
        try:
            assert isinstance(send_to, list)
            print(f"Sending email to {str(send_to)}")
            subject_date = datetime.strftime(datetime.now() + timedelta(hours=8), '%Y-%m-%d')
            #subject_date = datetime.strftime(datetime.now(), '%Y-%m-%d')
            subject = f"AMPOS CARE 每日報表 {subject_date}"
            msg = MIMEMultipart()
            msg['From'] = send_from
            msg['To'] = COMMASPACE.join(send_to)
            msg['Date'] = formatdate(localtime=True)
            print(formatdate(localtime=True))
            msg['Subject'] = subject
            
            with open(f"/tmp/{html_filename}", "r") as fil:    
                msg.attach(MIMEText(fil.read(), 'html'))
            
            '''
            with open(f"/tmp/{html_filename}", "rb") as fil:
                part = MIMEApplication(fil.read(), Name=f"{html_filename}")
            # After the file is closed
            part['Content-Disposition'] = f'attachment; filename="{html_filename}"'
            msg.attach(part)
            '''

            smtp = smtplib.SMTP(server)
            smtp.ehlo()
            smtp.starttls()
            print(smtp.login("AKIAJ6GS6FLLJRDDJHGA","AtlO08I3mpE4367nbv7fBkElPT20ImpbXNmaOubGht71"))
            smtp.sendmail(send_from, send_to, msg.as_string())  
            smtp.quit()
            smtp.close()
            return f"{subject} - {send_to} Email Sent Successfully\n"
        except Exception as e:
            return f"{subject} - {send_to} Email Sent Failed. Error message: {e}\n"
    else:
        return 'Email list is empty ...'

'''
def email_handler(event, context):
    #print(filter_latests())
    l = list(filter(lambda x: x['Key'].find('(王道銀行)') > -1, filter_latests()))
    print(l)
    build_body_html_page(l)
    send_email()
'''

def email_handler(event, context):
    print(event)
    if 'key' in event:
        if event['key'] == 'thisfunctionisdangeroussoitneedssomesimpleverificationincaseofaccidentallytriggeringthisfunctionandsendallemalstoallcustomerswhichisterrible':
            post_result('Daily survey report email sending triggered.')
            report_list = filter_latests()

            group_by_branch = {}
            for x in report_list:
                tag = s3_client.get_object_tagging(Bucket=BUCKET_NAME, Key=x['Key'])
                try:
                    branch_id_index = list(filter(lambda x: x['Key'] == 'branch_id', tag['TagSet']))[0]['Value']
                    branch_email_list = list(filter(lambda x: x['Key'] == 'email_list', tag['TagSet']))[0]['Value']
                except Exception as e:
                    print(f"Failed S3 key to be added into group_by_branch, object skipeped. Exception type: {e}")
                    print(x)
                    print(tag)
                
                if branch_id_index != 'NUL':
                    if branch_id_index not in group_by_branch:
                        group_by_branch[branch_id_index] = {"email_list": branch_email_list.split('+'), "report_links":[x]}
                    else:
                        group_by_branch[branch_id_index]['report_links'].append(x)
            
            #print(json.dumps(group_by_branch, indent=2, ensure_ascii=False))
            result_msg = ''
            for k in group_by_branch.keys():
                #print(group_by_branch[k]['report_links'])
                html_filename = build_body_html_page(group_by_branch[k]['report_links'])
                result_msg += send_email(group_by_branch[k]['email_list'], html_filename)
            post_result(result_msg)

def lambda_handler(event, context):
    print(event)
    
    if 'auto' in event:
        #build_body_html_page(filter_latests())
        #send_email()
        pass
    else:        
        default_query_value(event)
        if check_token(copy(event)):
            response = {
                "statusCode": 200,
                "headers": {
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Auth-Token",
                    "Access-Control-Allow-Methods": "GET,OPTIONS",
                    "Content-Type": content_type(copy(event))
                },
                "body": body(copy(event))
            }
        else:
            response = {
                "statusCode": 403,
                "body": "ACCESS DENIED"
            }

        return response