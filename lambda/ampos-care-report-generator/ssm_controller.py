#!/usr/bin/python3

import boto3

region = boto3.session.Session().region_name
ssm_client = boto3.client('ssm', region_name=region)

def get_ssm_parameter(parameter_name):
    parameter_response = ssm_client.get_parameter(Name=parameter_name)
    print(parameter_response)
    return parameter_response['Parameter']['Value'] 