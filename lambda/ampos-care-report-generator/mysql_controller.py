#!/usr/bin/python3

import mysql.connector
import json
from ssm_controller import get_ssm_parameter

config = {}

def fetch_db_info(domain, environment):
    # strickly no writing to the database, use read-only replica to fetch data until API function supported
    db_username = 'ampos_datalake'
    db_password = 'FzAMhpZ53KvsNrVB7muAdt6XeYwffW'
    db_schema = 'bluecrystalserver_ampos_care'
    if environment == 'prod-cn':
        db_endpoint = 'group1-cn-prod-read-replica.cvysmqlixrgl.rds.cn-north-1.amazonaws.com.cn'
    elif environment == 'qa-cn' or environment == 'dev-cn':
        pass
    elif environment == 'prod':
        db_endpoint = 'group1-sg-prod-read-replica.cezvr3lheyfj.ap-southeast-1.rds.amazonaws.com'
    elif environment == 'qa' or environment == 'dev':
        pass
    else:
        print('You should never hit this error')
    return db_username, db_password, db_endpoint, db_schema

# return a new cursor
def refresh_connection(domain, environment):
    print('Refreshing mysql connector ...')
    username, password, endpoint, database = fetch_db_info(domain, environment)
    try:
        cnx = mysql.connector.connect(
            user=username,
            password=password,
            host=endpoint,
            database=database
        )
    except mysql.connector.errors.ProgrammingError as e:
        print('Connecting to MySQL failed: %s' % e)
        cnx = None
    if cnx:
        print('mysql connector refreshed')
        return cnx