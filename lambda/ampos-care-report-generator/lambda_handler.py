#!/usr/bin/python3
import requests
import json
import boto3
from datetime import datetime as dt
from datetime import timedelta
from s3_controller import upload_item, tag_object
from openpyxl_handler import _spreadsheet_generator
from mysql_controller import refresh_connection

#ES Params
es_endpoint = 'https://search-bluecrystal-5yzlh44i627qnxcjqo7rotwzka.ap-southeast-1.es.amazonaws.com'
survey_index = 'ampos-care-prod_surveys'
batch_size = 5000
#RDS Params
domain = 'ampos-care'
environment = 'prod'
excel_chunk_size = 70000
timezone_offset = timedelta(hours=8)


def parse_survey_name(survey_name):
    survey_name_groups = survey_name.split(' ')
    if len(survey_name_groups) == 3: #Standard format : <title> (<company_name>) <date_number>
        print(survey_name_groups)
        survey_title = survey_name_groups[0]
        branch = survey_name_groups[1][1:-1]
        datenumber = survey_name_groups[2]
        return survey_title, branch, datenumber
    else:  # Any other format
        survey_title = survey_name_groups[0]
        return survey_title, 'NUL' , 'NUL'

def get_branch_list():
    url = f"https://api-{domain}.ampostech.com/api/branches?companyId=1&sort=id%2Casc&size=2000&page=0"
    headers = {"Content-Type": "application/json", "X-Auth-Token": "hr@ampos-care:1900954267514:4ad107d6852b6f4462ca5d66ad37b8d9"}
    res = requests.get(url, headers=headers)
    branch_map_list = {}
    for b in res.json():
        if "address" in b:
            branch_map_list[b['name']] = { "id": b['id'], "address": b['address']}
        else:
            branch_map_list[b['name']] = { "id": b['id'], "address": 'NUL'}
    return branch_map_list

def get_branch_id(branch_name, branch_map_list):
    if branch_name in branch_map_list:
        return branch_map_list[branch_name]['id']
    else:
        return 'NUL'

def get_branch_address(branch_name, branch_map_list):
    if branch_name in branch_map_list:
        return branch_map_list[branch_name]['address']
    else:
        return 'NUL'

def survey_report_handler(event, context):
    print(event)
    print(context)
    branch_map_list = get_branch_list()
    try:
        for survey in get_today_yesterday_survey():
            survey_id = survey[0]
            survey_name = survey[1]
            survey_name_groups = parse_survey_name(survey_name)
            a = get_survey_result(survey_id)
            print(f"Numbers of survey collected: {str(len(a))}")
            output_excel(survey_id, survey_name, convert_to_csv(survey_id, a))
            filename = f'{survey_name}.xlsx'
            print(f"{filename} finished report generating.")
            upload_item(filename, 'ampos-care-report/survey_result/', archived_s3_key='ampos-care-report/survey_result/archived/')
            branch_trantable = survey_name.maketrans('', '', '()#')
            tag_object(filename, 'ampos-care-report/survey_result/', tag_set=[{'Key': 'branch', 'Value': survey_name_groups[1].translate(branch_trantable)}, {'Key': 'branch_id', 'Value': str(get_branch_id(survey_name_groups[1], branch_map_list)).translate(branch_trantable)}, {'Key': 'email_list', 'Value': get_branch_address(survey_name_groups[1], branch_map_list).translate(branch_trantable)}])

        '''
        # 未完成健康管理登記人員
        cnx = refresh_connection(domain, environment)
        latest_survey_id = get_today_yesterday_survey()[-1]
        print(f"Query survey_not_done. Latest survey id : {latest_survey_id}")
        query = open('query/survey_not_done.sql', 'r').read().replace('{survey_id}', str(latest_survey_id))
        cursor = cnx.cursor()
        cursor.execute(query)
        field_names = _make_field_names(cursor.description)
        db_fetch = cursor.fetchall()
        db_fetch.insert(0, field_names)
        print(len(db_fetch))        
        filename = '未完成健康管理登記人員.xlsx'
        _spreadsheet_generator(db_fetch, ws_title='未完成健康管理登記人員', load=False, filename_to_load=filename).save('/tmp/%s' % filename)
        print(filename)
        upload_item(filename, 'ampos-care-special-report/', archived_s3_key='ampos-care-special-report/archived/')
        '''

    except:
        print('Bad event syntax')
        raise

def get_today_yesterday_survey():
    today = dt.today()
    day_delta = timedelta(days=1)
    survey_criteria_date_yesterday = dt.strftime(today + timezone_offset - day_delta, '%Y-%m-%d')
    survey_criteria_date_today = dt.strftime(today + timezone_offset, '%Y-%m-%d')
    print('Survey criteria date (yesterday) TO (today): ', survey_criteria_date_yesterday, survey_criteria_date_today)
    cnx = refresh_connection(domain, environment)
    query = open('query/today_yesterday_survey.sql', 'r').read().replace('{yesterday}', survey_criteria_date_yesterday).replace('{today}', survey_criteria_date_today)
    cursor = cnx.cursor()
    cursor.execute(query)
    db_fetch = cursor.fetchall()
    output = []
    print('Survey names and ID that are going to be process in this session ...')
    for row in db_fetch:
        print(row[1])
        output.append((row[0], row[1]))
    print(output)
    return output

def _make_field_names(description):
    field_names = [i[0] for i in description]
    return tuple(field_names)

def get_survey_result(survey_id):
    output = []
    
    headers = {"Content-Type": "application/json"}
    body = {"size":batch_size,"_source":["id","duration","startDate","finishDate","survey.*","result.*","user.id","user.login","user.firstName","user.lastName","user.userProfile.branches","user.userProfile.jobTitle.name","user.userProfile.supervisorId","user.userProfile.departmentId","user.mobilePhone","finishDate"],"sort":[{"survey.id":{"order":"asc"}}],"query":{"bool":{"filter":[{"range":{"survey.id":{"from":survey_id,"to":survey_id}}}]}}}
    body = json.dumps(body)
    res = requests.post(f'{es_endpoint}/{survey_index}/_search?scroll=1m', headers=headers, data=body)
    lookup_table = generate_lookup(survey_id)
    for record in res.json()['hits']['hits']:
        output.append(format_es_result_dict(record['_source'], lookup_table))
    scroll_body = {"scroll" : "1m","scroll_id" : res.json()['_scroll_id']}
    scroll_body = json.dumps(scroll_body)
    print(res)
    while res.json()['hits']['hits']:
        res = requests.post(f'{es_endpoint}/_search/scroll', headers=headers, data=scroll_body)
        lookup_table = generate_lookup(survey_id)
        for record in res.json()['hits']['hits']:
            output.append(format_es_result_dict(record['_source'], lookup_table))
        print(res)
    #print(json.dumps(res.json(), indent=2, ensure_ascii=False))
    return output

def format_es_result_dict(es_result, lookup_table):
    question_result = get_question_result(es_result['result'])
    try:
        lookup_key = str(es_result['user']['id'])
        output = { 
                    "jhi_id": es_result['user']['id'],
                    "登入ID": es_result['user']['login'],
                    "員工姓名": es_result['user']['lastName'] + es_result['user']['firstName'],
                    #"电话号码": es_result['user']['mobilePhone'],
                    #"BU": lookup_table[lookup_key]['business_unit'],
                    #"职称": lookup_table[lookup_key]['job_title'],
                    #"直属上级工号": lookup_table[lookup_key]['supervisor_login'],
                    #"提交时间": dt.strptime(es_result['finishDate'], '%Y-%m-%dT%H:%M:%S.%f%z'),
                    "提交時間": lookup_table[lookup_key]['submitted_date']
                }
    except KeyError as e:
        print('*******ERROR: ')
        print(es_result['user']['login'], ': Key = ', end=' ')
        print(e)
        output = {}
    output = {**output, **question_result}
    return output

def get_question_result(result):
    output = {}
    for question in result:
        try:
            answer = [x['text'].strip().replace('\n', '').replace(',', '.') for x in question['answerText']]
            answer = ';'.join(answer)
            output[question['title'].strip()] = answer
        except:
            raise
            print('bad answer response')
            print(question)
            output[question['title'].strip()] = 'N/A'
    return output

def generate_lookup(survey_id):
    cnx = refresh_connection(domain, environment)
    print(f'Generating look up table for survey id: {survey_id}')
    query = open('query/lookup_table.sql', 'r').read().replace('{survey_id}', str(survey_id))
    cursor = cnx.cursor()
    cursor.execute(query)
    field_names = _make_field_names(cursor.description)
    db_fetch = cursor.fetchall()
    db_fetch.insert(0, field_names)
    print(f"Lookup table generated .. length : {len(db_fetch)-1}")
    header = list(db_fetch[0])
    print(header)
    output = {}
    for row in db_fetch[1:]:
        key = str(row[0])
        output[key] = {}
        for x in range(len(header)):
            output[key][header[x]] = row[x]
    return output

def convert_to_csv(survey_id, processed_dict):
    timestamp_pattern = '%m%d%H%M'
    output = ''
    f = open(f'/tmp/survey_result_{survey_id}_{dt.strftime(dt.now(), timestamp_pattern)}.csv', 'w')
    header = ''
    if processed_dict:
        for key in processed_dict[0].keys():
            if key not in ['jhi_id']:
                header += f'{key},'
        header = header[:-1] + '\n'
    else:
        header = 'EMPTY SURVEY'
    print(header)
    f.write(header)
    output += header

    if processed_dict:
        for result in processed_dict:
            row = ''
            for key in header.replace('\n', '').split(','):
                row += f'{result[key]},'
            row = row[:-1] + '\n'
            f.write(row)
            output += row    
    return output

def output_excel_daily_healthcheck(survey_id, survey_name, csv_str):
    print('Output excel special case: output_excel_daily_healthcheck')
    if csv_str == 'EMPTY SURVEY':
        rows_with_symp = [('今日尚未有人填寫＜每日自我防疫檢疫調查＞ ...','')]
        rows_without_symp = [('今日尚未有人填寫＜每日自我防疫檢疫調查＞ ...','')]
    else:
        rows = csv_str.split('\n')
        header = tuple(rows[0].split(','))
        rows_with_symp = [header]
        rows_without_symp = [header]
        for r in rows[1:-1]:
            if r.split(',')[3] == '無任何症狀':
                rows_without_symp.append(tuple(r.split(',')))
            else:
                rows_with_symp.append(tuple(r.split(',')))
    
    filename = survey_name
    _spreadsheet_generator(rows_with_symp, ws_title='有症狀人員清單', load=False, filename_to_load=filename).save('/tmp/%s.xlsx' % filename) #lambda tmp path
    _spreadsheet_generator(rows_without_symp, ws_title='無症狀人員清單', load=True, filename_to_load='/tmp/%s.xlsx' % filename).save('/tmp/%s.xlsx' % filename)

def output_excel(survey_id, survey_name, csv_str):
    if survey_name.find('每日自我防疫檢疫調查') > -1:
        output_excel_daily_healthcheck(survey_id, survey_name, csv_str)
    else:
        rows = csv_str.split('\n')
        rows = list(map(lambda x: tuple(x.split(',')), rows))
        filename = survey_name
        _spreadsheet_generator(rows, ws_title=filename[:31], load=False, filename_to_load=filename).save('/tmp/%s.xlsx' % filename) #lambda tmp path

#survey_report_handler({}, {})