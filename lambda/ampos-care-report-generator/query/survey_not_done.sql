SELECT
survey.title AS 'Survey_Title', ju.login, CONCAT_WS(' ',ju.last_name, ju.first_name) AS name, 
 bu.name AS 'Business_Unit',
sup_ju.login AS 'Supervisor',
-- IDL
CASE
WHEN upb.br LIKE '%10026%' THEN 'Yes'
ELSE 'No'
END AS 'Belongs_to_IDL',
ju.mobile_phone, sa.submitted_date

FROM bluecrystalserver_ampos_care.survey_assignment AS sa

LEFT JOIN bluecrystalserver_ampos_care.survey ON sa.survey_id = survey.id
LEFT JOIN bluecrystalserver_ampos_care.jhi_user AS ju ON sa.user_id = ju.user_profile_id
LEFT JOIN bluecrystalserver_ampos_care.user_profile AS up ON ju.user_profile_id = up.id
LEFT JOIN bluecrystalserver_ampos_care.business_unit AS bu ON up.business_unit_id = bu.id
-- Supervisor
LEFT JOIN bluecrystalserver_ampos_care.jhi_user AS sup_ju ON up.supervisor_id = sup_ju.user_profile_id
-- IDL
LEFT JOIN ( SELECT user_profile_id, group_concat(branch_id) AS br FROM bluecrystalserver_ampos_care.user_profile_branch GROUP BY user_profile_id
    ) AS upb ON up.id = upb.user_profile_id
WHERE submitted_date IS NULL AND sa.survey_id = {survey_id}	#--Modified--
AND ju.id not in (1, 2, 3, 4, 5, 10034, 10035, 10036, 10037, 10038, 10039, 10040, 10041, 10042, 10043, 10044, 10045, 13927, 14055)
ORDER BY sa.survey_id , sa.assigned_date, Business_unit, sa.submitted_date DESC;