SELECT ju.id, ju.login, sa.submitted_date + INTERVAL 1 HOUR AS 'submitted_date', bu.name AS 'business_unit', jt.name AS 'job_title', jus.login AS 'supervisor_login'
FROM survey_assignment sa
LEFT JOIN jhi_user ju ON ju.id = sa.user_id
LEFT JOIN user_profile up ON ju.user_profile_id = up.id
LEFT JOIN business_unit bu ON bu.id = up.business_unit_id
LEFT JOIN job_title jt ON jt.id = up.job_title_id
LEFT JOIN jhi_user jus ON jus.id = up.supervisor_id
WHERE survey_id = {survey_id} AND submitted_date IS NOT NULL;