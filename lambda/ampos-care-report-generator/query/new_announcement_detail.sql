SELECT ac.name AS 'Announcement_Channel_Name',  ah.created_by AS 'Reader_Login', bu.name AS 'Business_Unit', sup_ju.login AS 'Supervisor',
				jt.name AS 'Job_title', ju.mobile_phone, a.id AS 'Announcement ID', a.subject AS 'Subject', a.message AS 'Message', #ah.reader,
				CASE
				  WHEN ah.has_ack = 0 THEN 'No'
                  WHEN ah.has_ack = 1 THEN 'Yes'
				END AS 'Has Acknowledged',
                #branch.name
                CASE
				  WHEN upb.br LIKE '%10026%' THEN 'Yes'
                  ELSE 'No'
				END AS 'Belongs_to_IDL'
				#a.created_by AS 'Created By', a.created_date AS 'Created date'
FROM bluecrystalserver_tpk.announcement_history ah
	JOIN bluecrystalserver_tpk.announcement a ON ah.announcement_id = a.id
    JOIN bluecrystalserver_tpk.announcement_channel ac ON ac.announcement_room_id = a.channel
    LEFT JOIN ( SELECT user_profile_id, group_concat(branch_id) AS br
			FROM bluecrystalserver_tpk.user_profile_branch
            GROUP BY user_profile_id
    ) AS upb ON ah.reader = upb.user_profile_id
    #---------------------------------------------------
    LEFT JOIN bluecrystalserver_tpk.user_profile AS up ON ah.reader = up.id
    LEFT JOIN bluecrystalserver_tpk.business_unit AS bu ON up.business_unit_id = bu.id
    LEFT JOIN bluecrystalserver_tpk.jhi_user AS ju ON up.id = ju.user_profile_id
    -- Supervisor
    LEFT JOIN bluecrystalserver_tpk.jhi_user AS sup_ju ON up.supervisor_id = sup_ju.user_profile_id
    -- Job title
    LEFT JOIN bluecrystalserver_tpk.job_title AS jt ON up.job_title_id = jt.id
    #LEFT JOIN bluecrystalserver_tpk.branch ON upb.branch_id = branch.id
WHERE a.id >=903 AND ah.has_read=1 AND
 ah.reader not in (1, 2, 3, 4, 5, 10034, 10035, 10036, 10037, 10038, 10039, 10040, 10041, 10042, 10043, 10044, 10045, 13927, 14055)
ORDER BY a.created_date desc;