SELECT 
	/*
    CASE
		WHEN SUM(CASE WHEN lp.lesson_status = 3 THEN 1 WHEN lp.lesson_status = 4 THEN 1 ELSE 0 END) != COUNT(t1.lesson_id) THEN '未完成'
		ELSE MAX(lp.finished_date) 
	END AS '完成时间',	
    */
	t1.course_name AS '课程名称',
	t1.login AS '员工工号', 
	CONCAT(t1.last_name, t1.first_name) AS '员工姓名',
    jt.name AS '员工职称',
    ju_supervisor.login AS '员工直属上级工号',
    GROUP_CONCAT(DISTINCT b.name SEPARATOR '@') AS '员工单位',
	bu.name AS '员工BU',
    up.region AS '员工所在区域',
    CASE
		WHEN SUM(CASE WHEN lp.lesson_status = 3 THEN 1 WHEN lp.lesson_status = 4 THEN 1 ELSE 0 END) = COUNT(t1.lesson_id) THEN '已完成'
        WHEN SUM(CASE WHEN lp.lesson_status = 3 THEN 1 WHEN lp.lesson_status = 4 THEN 1 ELSE 0 END) > 0 THEN '进行中'
		ELSE '尚未开始'
	END AS '课程完成状态',
	CASE
		WHEN MIN(lp.started_date) IS NOT NULL THEN MIN(lp.started_date)
		ELSE '尚未开始'
	END AS '开始时间',
    t1.permission_category AS '课程权限类型'
FROM (
	SELECT c.id AS 'course_id', c.name AS 'course_name', l.id AS 'lesson_id', l.name AS 'lesson_name',  egm.user_id, ju.login, ju.first_name, ju.last_name, 
    ju.user_profile_id, 'Predefined Group' AS 'permission_category', l.minimum_score
	FROM lesson l
		JOIN course c ON l.course_id = c.id
		JOIN subscribe_log sl ON sl.subscriber_id = c.id AND sl.subscriber_type = 'Course'
		JOIN evaluated_group_member egm ON egm.predefined_group_id = sl.publisher_id AND sl.publisher_type = 'PredefinedGroup'
		JOIN jhi_user ju ON ju.id = egm.user_id
	WHERE ju.activated = 1 AND ju.id > 5 AND c.deleted_date IS NULL AND l.deleted_date
	UNION
	SELECT c.id AS 'course_id', c.name AS 'course_name', l.id AS 'lesson_id', l.name AS 'lesson_name', job_attribute_user.id, 
		   job_attribute_user.login, job_attribute_user.first_name, job_attribute_user.last_name, job_attribute_user.user_profile_id,
           'Job Attribute' AS 'permission_category', l.minimum_score
	FROM lesson l
		LEFT JOIN course c ON l.course_id = c.id
		JOIN acl_object_identity aoi ON c.id = aoi.object_id_identity AND aoi.object_id_class = 5
		LEFT JOIN acl_entry ae ON ae.acl_object_identity = aoi.id AND ae.granting = 1
		LEFT JOIN acl_sid asid ON ae.sid = asid.id
		JOIN (SELECT ju.id, ju.login, ju.first_name, ju.last_name, ju.user_profile_id, CONCAT('PERM_JOB_ATTRIBUTE_', ja.id) AS 'job_attribute_id'
			  FROM jhi_user ju
				LEFT JOIN user_profile up ON ju.user_profile_id = up.id
				LEFT JOIN job_title jt ON up.job_title_id = jt.id
				JOIN job_attribute ja ON jt.job_attribute_id = ja.id
			  WHERE ju.activated = 1 AND ju.id > 5) job_attribute_user ON job_attribute_user.job_attribute_id = asid.sid
	WHERE c.deleted_date IS NULL AND l.deleted_date IS NULL
		AND (l.id, job_attribute_user.id) NOT IN (
			SELECT l.id, egm.user_id
			FROM lesson l
				JOIN course c ON l.course_id = c.id
				JOIN subscribe_log sl ON sl.subscriber_id = c.id AND sl.subscriber_type = 'Course'
				JOIN evaluated_group_member egm ON egm.predefined_group_id = sl.publisher_id AND sl.publisher_type = 'PredefinedGroup'
				JOIN jhi_user ju ON ju.id = egm.user_id
			WHERE ju.activated = 1 AND ju.id > 5)
    UNION
	SELECT c.id AS 'course_id', c.name AS 'course_name', l.id AS 'lesson_id', l.name AS 'lesson_name', all_company_activated.id, 
		   all_company_activated.login, all_company_activated.first_name, all_company_activated.last_name, all_company_activated.user_profile_id,
           'All Company' AS 'permission_category', l.minimum_score
	FROM lesson l
		LEFT JOIN course c ON l.course_id = c.id
		JOIN acl_object_identity aoi ON c.id = aoi.object_id_identity AND aoi.object_id_class = 5
		LEFT JOIN acl_entry ae ON ae.acl_object_identity = aoi.id AND ae.granting = 1
		LEFT JOIN acl_sid asid ON ae.sid = asid.id
		JOIN (SELECT ju.id, ju.login, ju.first_name, ju.last_name, ju.user_profile_id, 'PERM_COMPANY_1' AS 'all_company_perm'
			  FROM jhi_user ju
				LEFT JOIN user_profile up ON ju.user_profile_id = up.id
			  WHERE ju.activated = 1 AND ju.id > 5) all_company_activated ON all_company_activated.all_company_perm = asid.sid
	WHERE c.deleted_date IS NULL AND l.deleted_date IS NULL
		AND (l.id, all_company_activated.id) NOT IN (
			SELECT l.id, egm.user_id
			FROM lesson l
				JOIN course c ON l.course_id = c.id
				JOIN subscribe_log sl ON sl.subscriber_id = c.id AND sl.subscriber_type = 'Course'
				JOIN evaluated_group_member egm ON egm.predefined_group_id = sl.publisher_id AND sl.publisher_type = 'PredefinedGroup'
				JOIN jhi_user ju ON ju.id = egm.user_id
			WHERE ju.activated = 1 AND ju.id > 5)    
	) t1
	LEFT JOIN user_profile up ON up.id = t1.user_profile_id
    LEFT JOIN user_profile_branch upb ON upb.user_profile_id = up.id
    LEFT JOIN branch b ON b.id = upb.branch_id
    LEFT JOIN job_title jt ON jt.id = up.job_title_id
    LEFT JOIN department d ON d.id = up.department_id
    LEFT JOIN business_unit bu ON bu.id = up.business_unit_id
    LEFT JOIN region r ON r.id = b.region_id
    LEFT JOIN jhi_user ju_supervisor ON ju_supervisor.id = up.supervisor_id
    LEFT JOIN lesson_progress lp ON lp.lesson_id = t1.lesson_id AND lp.user_id = t1.user_id
GROUP BY t1.login, t1.course_id
ORDER BY t1.user_id, t1.course_id