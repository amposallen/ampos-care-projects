SELECT uer.reference_id AS 'Reference ID', 
	ju.login AS 'Employee ID', 
    ju.email AS 'Email',
    ju.phone_country_code AS 'Phone Country Code', 
    ju.mobile_phone AS 'Mobile', 
    sp.login AS 'Supervisor ID', 
    sp.login AS 'Supervisor', 
    ju.last_name AS 'Last Name', 
    ju.first_name AS 'First Name', 
    ju.display_name AS 'Preferred Name', 
    up.birthday AS 'Date of Birth', 
    CASE
		WHEN up.gender = 0 THEN 'MALE'
        WHEN up.gender = 1 THEN 'FEMALE'
        WHEN up.gender = 2 THEN 'NOT_SPECIFIED'
	END AS 'Gender',
	GROUP_CONCAT(DISTINCT(b.name) SEPARATOR '@') AS 'Branch',
    d.name AS 'Department',
    jt.name AS 'Job Title',
    jter.reference_id AS 'Job Title Reference Id',
    up.address AS 'Address',
    up.city AS 'City',
    up.province AS 'Province',
    up.postal_code AS 'Postal Code',
    up.hire_date AS 'Hire Date',
    GROUP_CONCAT(DISTINCT(ug.name) SEPARATOR '@') AS 'User Group List',
    CASE
		WHEN ju.activated = 0 THEN 'FALSE'
        WHEN ju.activated = 1 THEN 'TRUE'
	END AS 'Activate',
    up.employment_type AS 'Employment Type',
    up.leave_date AS 'Leave Date',
    NULL AS 'Modified Date',
    bu.name AS 'Business Unit',
    up.region AS 'Region'
FROM jhi_user ju
LEFT JOIN user_external_reference uer ON ju.id = uer.user_id
LEFT JOIN user_profile up ON up.id = ju.id
LEFT JOIN (SELECT up.id, ju.login FROM user_profile up JOIN jhi_user ju ON up.supervisor_id = ju.id) sp ON sp.id = ju.id
LEFT JOIN user_profile_branch upb ON ju.user_profile_id = upb.user_profile_id
LEFT JOIN branch b ON b.id = upb.branch_id
LEFT JOIN department d ON d.id = up.department_id
LEFT JOIN job_title jt ON jt.id = up.job_title_id
LEFT JOIN job_title_external_reference jter ON jter.job_title_id = jt.id
LEFT JOIN user_profile_user_group upug ON upug.user_profiles_id = ju.user_profile_id
LEFT JOIN user_group ug ON ug.id = upug.user_groups_id
LEFT JOIN business_unit bu ON bu.id = up.business_unit_id = bu.id
WHERE ju.id > 5 and ju.activated=1
GROUP BY ju.login
ORDER BY ju.id;

/*
Reference ID
Employee ID
Email
Phone Country Code
Mobile
Supervisor ID
Supervisor
Last Name
First Name
Preferred Name
Date of Birth
Gender
Branch
Department
Job Title
Job Title Reference Id
Address
City
Province
Postal Code
Hire Date
User Group List
Activate
Employment Type
Leave Date
Modified Date
Business Unit
Region
CC_Desc
Group Ke
Group Bu
Group Chu
Reason of Updating
*/