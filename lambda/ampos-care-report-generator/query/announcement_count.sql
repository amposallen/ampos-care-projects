SELECT ac.name AS 'announcement_channel_name', a.id, a.subject, a.message, a.created_by,
a.created_date, SUM(ah.has_read) AS 'read_count', SUM(ah.has_ack) AS 'acknowledged_count',
 a.enable_ack FROM bluecrystalserver_tpk.announcement_history ah
 RIGHT JOIN bluecrystalserver_tpk.announcement a ON ah.announcement_id = a.id
 JOIN bluecrystalserver_tpk.announcement_channel ac ON ac.announcement_room_id = a.channel
 WHERE a.id>0 AND ah.reader not in (1, 2, 3, 4, 5, 10034, 10035, 10036, 10037, 10038, 10039, 10040, 10041, 10042, 10043, 10044, 10045, 13927, 14055)
 AND announcement_id > 902
 GROUP BY a.id
 ORDER BY a.created_date desc;
