# non-login
SELECT ju.login AS '员工工号', b.name AS 'BU', bu.name AS 'SBU', jt.name AS '职称', 
	CONCAT(ju.last_name, ju.first_name) AS '员工姓名', sup_ju.login AS '员工直属上级工号' , ju.mobile_phone  AS '员工电话'
FROM bluecrystalserver_tpk.jhi_user AS ju
	LEFT JOIN bluecrystalserver_tpk.user_profile up ON ju.user_profile_id = up.id
	LEFT JOIN bluecrystalserver_tpk.job_title jt ON up.job_title_id = jt.id
	LEFT JOIN bluecrystalserver_tpk.user_profile_branch upb ON upb.user_profile_id = ju.user_profile_id
    LEFT JOIN bluecrystalserver_tpk.branch b ON upb.branch_id = b.id #assume 1 to 1 mapping (user --- branch)
    LEFT JOIN bluecrystalserver_tpk.business_unit AS bu ON up.business_unit_id = bu.id
	LEFT JOIN bluecrystalserver_tpk.jhi_user AS sup_ju ON up.supervisor_id = sup_ju.user_profile_id  
WHERE ju.activated = 1 AND ju.id > 5
	AND ju.id NOT IN(SELECT act_t.user_id 
					 FROM bluecrystalserver_tpk.action_tracking AS act_t
					 WHERE action_timestamp >= '2020-01-29 00:00:00' 
                     GROUP BY user_id) 
	AND ju.login NOT IN ('testwebadmin','testanson','testchieh','testdl','testhigh','testhr','testjj','testmid','hr002','hr001','michaelchiang','fosterchiang','deniseliu','shuhuanlin','tpkhr','hr1','hr2','hr3')
ORDER BY b.name, ju.login ASC;