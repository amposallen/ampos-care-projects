#!/usr/bin/python3

import boto3
import json
from datetime import datetime
from copy import copy

region = boto3.session.Session().region_name
s3_client = boto3.client('s3', region_name=region)

BUCKET_HARD_CODED = 'customer-sync-sg.ampostech.com'

def upload_item(filename, s3_key, archived_s3_key=''):
    try:
        path = '%s%s' % (s3_key, filename)
        s3_client.upload_file('/tmp/%s' % filename, BUCKET_HARD_CODED, path)
        print('%s upload to s3 bucket=%s, path=%s successed!' % (filename, BUCKET_HARD_CODED, path))
        if archived_s3_key:
            archived_path = '%s%s' % (archived_s3_key, filename.replace('.xlsx', '_%s.xlsx' % datetime.now().strftime('%Y%m%d%H%M%S')))
            s3_client.upload_file('/tmp/%s' % filename, BUCKET_HARD_CODED, archived_path)
            print('%s upload to s3 bucket=%s, path=%s successed!' % (filename, BUCKET_HARD_CODED, archived_path))
        return True
    except:
        print('s3_client upload report item to s3 error occurred (1)')
        raise

'''
## example of tag_set ##
[
   {
     'Key': 'string',
     'Value': 'string'
   }
]
'''

def tag_object(filename, s3_key, tag_set=[]):
    path = '%s%s' % (s3_key, filename)
    print(path)
    Tagging = {}    
    Tagging['TagSet'] = copy(tag_set)
    Tagging['TagSet'].append({ "Key": "timestamp", "Value": datetime.now().strftime('%Y%m%d%H%M%S')})
    try:
        print(Tagging)
        s3_client.put_object_tagging(Bucket=BUCKET_HARD_CODED, Key=path, Tagging=Tagging)
        return True
    except:
        print('s3_client tag object error occurred (1)')
        raise