#!/usr/bin/python3

import time
import datetime
import codecs
import json
import re
import csv
import time
import unicodedata
import openpyxl as xl
from openpyxl.styles import NamedStyle, Font, Border, Side, Alignment

# iterate row and get the max length value for each column
def count_max_len(col_max_length, index, row_value):
    while len(col_max_length) < index+1:
        col_max_length.append(0)
    try:
        # in case the value is not str (e.g. long) --> convert to unicode
        length = len(unicode(row_value)) + real_width(unicode(row_value))
    except:
        # in case the value is in utf-8 (over 128 range), unicode() cannot directly 
        # conver them so then decode them with utf-8.
        length = len(str(row_value)) + real_width(str(row_value))
    if length > col_max_length[index]:
        col_max_length[index] = length
    return col_max_length

def adjust_column_width(col_max_length, ws, wrap_text):
    # adjust col width accorfing to the max length;
    # and wraptext align 
    wrapText = NamedStyle(name="wrapText")
    wrapText.alignment = Alignment(wrapText=True)
    k = 0 # number index coreesponded to col for col_max_length
    for col in ws.columns:
        column_index = col[0].column #excel column index e.g. A B C D ... AA ...
        ws.column_dimensions[column_index].width = col_max_length[k] + 2.5
        if ws.column_dimensions[column_index].width > 60:
            ws.column_dimensions[column_index].width = 60
            if wrap_text:
                for cell in ws[column_index]:
                    cell.style = wrapText
        elif ws.column_dimensions[column_index].width < 2:
            ws.column_dimensions[column_index].width = 5
        if k + 1 > len(col_max_length):
            break
        else:
            k += 1
    return ws

'''
calculate a real with for a string: add width=2 for east asian characters
(Chinese characters)
'''
def real_width(content):
    str_width = 0
    #determine if there is wide char in a cell (Note:　Chinese is wide char)
    for char in content:
        if unicodedata.east_asian_width(char) == 'W':
            str_width += 1
    return str_width

'''helper function for generating excel file for other report methods to invoke. 

    if parameter `load` is false, the method will create a new workBook, otherwise
    filename_to_load must be specify correctly so the method will load the given 
    excel file

    ws_title determines the sheet name. Only 1 sheet will be process for this method
    (so if you looking for processing multiple sheet you can try start from this method
    multiple times). If start a new workbook and ws_title specified, ws_title will be
    name of worksheet. If load a new workbook and ws_title specified. this function will
    look up the name in the worksheet list, if found, use it, otherwise insert a new one
    and use it. Please note that using the existed worksheet means rewrite the worksheet

    (str, str, bool, str) => xl.workBook(), list'''
def _spreadsheet_generator(query_result, customizable_caller=None, ws_title='', load=False, filename_to_load='', wrap_text=False):
    raw = query_result
    print('Generating spreadsheet ... ')
    if not load:
        wb = xl.Workbook()
        ws = wb.active
        if ws_title:
            ws.title = ws_title
    else:
        try:
            print(f"{filename_to_load}")
            wb = xl.load_workbook(filename_to_load)            
            if ws_title in wb.sheetnames:
                wb.active = wb.sheetnames.index(ws_title)
            else:
                wb.create_sheet(ws_title) if ws_title != '' else wb.create_sheet()
                wb.active = len(wb.sheetnames)-1
            ws = wb.active
        except:
            print('Invalid filename to load')
    defect = []
    col_max_length = [5]*20
    # vvvv any action that need to apply to elements (cell) do it here vvvv
    for row in raw:
        row = list(row)
        for i in range(len(row)):
            if isinstance(row[i], datetime.datetime):
                row[i] = time.strftime('%Y-%m-%d %H:%M:%S', row[i].timetuple())
            elif row[i] == None:
                row[i] = '#N/A'
            col_max_length = count_max_len(col_max_length, i, row[i])
        # a passing function that handles customized rows data
        if customizable_caller != None: customizable_caller(row)
        row = tuple(row)
        try:
            ws.append(row)
        except UnboundLocalError as e:
            print(e)
            raise
        except:
            defect.append(row[0])
    # adjust column width
    ws = adjust_column_width(col_max_length, ws, wrap_text)
    if defect:
        print('defect announcement id: %s' % str(defect)) 
    return wb